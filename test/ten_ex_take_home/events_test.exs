defmodule TenExTakeHome.EventsTest do
  use TenExTakeHome.DataCase

  alias TenExTakeHome.Events

  describe "event_records" do
    alias TenExTakeHome.Events.EventRecord

    import TenExTakeHome.EventsFixtures

    @invalid_attrs %{uri: nil, api: nil, page: nil, accessed_at: nil}

    test "list_event_records/0 returns all event_records" do
      event_record = event_record_fixture()
      assert Events.list_event_records() == [event_record]
    end

    test "get_event_record!/1 returns the event_record with given id" do
      event_record = event_record_fixture()
      assert Events.get_event_record!(event_record.id) == event_record
    end

    test "create_event_record/1 with valid data creates a event_record" do
      valid_attrs = %{uri: "some uri", api: "some api", page: "some page", accessed_at: ~N[2023-07-20 21:48:00]}

      assert {:ok, %EventRecord{} = event_record} = Events.create_event_record(valid_attrs)
      assert event_record.uri == "some uri"
      assert event_record.api == "some api"
      assert event_record.page == "some page"
      assert event_record.accessed_at == ~N[2023-07-20 21:48:00]
    end

    test "create_event_record/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Events.create_event_record(@invalid_attrs)
    end

    test "update_event_record/2 with valid data updates the event_record" do
      event_record = event_record_fixture()
      update_attrs = %{uri: "some updated uri", api: "some updated api", page: "some updated page", accessed_at: ~N[2023-07-21 21:48:00]}

      assert {:ok, %EventRecord{} = event_record} = Events.update_event_record(event_record, update_attrs)
      assert event_record.uri == "some updated uri"
      assert event_record.api == "some updated api"
      assert event_record.page == "some updated page"
      assert event_record.accessed_at == ~N[2023-07-21 21:48:00]
    end

    test "update_event_record/2 with invalid data returns error changeset" do
      event_record = event_record_fixture()
      assert {:error, %Ecto.Changeset{}} = Events.update_event_record(event_record, @invalid_attrs)
      assert event_record == Events.get_event_record!(event_record.id)
    end

    test "delete_event_record/1 deletes the event_record" do
      event_record = event_record_fixture()
      assert {:ok, %EventRecord{}} = Events.delete_event_record(event_record)
      assert_raise Ecto.NoResultsError, fn -> Events.get_event_record!(event_record.id) end
    end

    test "change_event_record/1 returns a event_record changeset" do
      event_record = event_record_fixture()
      assert %Ecto.Changeset{} = Events.change_event_record(event_record)
    end
  end
end
