defmodule TenExTakeHome.EventsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `TenExTakeHome.Events` context.
  """

  @doc """
  Generate a event_record.
  """
  def event_record_fixture(attrs \\ %{}) do
    {:ok, event_record} =
      attrs
      |> Enum.into(%{
        uri: "some uri",
        api: "some api",
        page: "some page",
        accessed_at: ~N[2023-07-20 21:48:00]
      })
      |> TenExTakeHome.Events.create_event_record()

    event_record
  end
end
