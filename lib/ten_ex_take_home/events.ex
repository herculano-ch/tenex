defmodule TenExTakeHome.Events do
  @moduledoc """
  The Events context.
  """

  import Ecto.Query, warn: false
  alias TenExTakeHome.Repo

  alias TenExTakeHome.Events.EventRecord

  @doc """
  Returns the list of event_records.

  ## Examples

      iex> list_event_records()
      [%EventRecord{}, ...]

  """
  def list_event_records do
    Repo.all(EventRecord)
  end

  @doc """
  Creates a event_record.

  ## Examples

      iex> create_event_record(%{field: value})
      {:ok, %EventRecord{}}

      iex> create_event_record(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event_record(attrs \\ %{}) do
    %EventRecord{}
    |> EventRecord.changeset(attrs)
    |> Repo.insert()
  end
end
