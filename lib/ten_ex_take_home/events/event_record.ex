defmodule TenExTakeHome.Events.EventRecord do
  use Ecto.Schema
  import Ecto.Changeset

  schema "event_records" do
    field :uri, :string
    field :api, :string
    field :page, :string
    field :accessed_at, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(event_record, attrs) do
    event_record
    |> cast(attrs, [:api, :uri, :page, :accessed_at])
    |> validate_required([:uri, :accessed_at])
  end
end
