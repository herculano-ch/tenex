defmodule TenExTakeHome.URLData do
  defstruct uri: nil, api: nil, page: %{}, accessed_at: nil

  def to_map(page) do
    uri = URI.parse(page)
    query = URI.decode_query(uri.query || "")
    page_encoded = Jason.encode!(%{limit: Map.get(query, "limit", "20"), offset: Map.get(query, "offset", "0")})

    %{
      api: uri.host,
      uri: uri.path,
      page: page_encoded,
      accessed_at: DateTime.utc_now()
    }
  end
end
