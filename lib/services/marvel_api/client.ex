defmodule TenExTakeHome.MarvelApiClient do
  @base_url "http://gateway.marvel.com/v1/public/characters"
  alias TenExTakeHome.MarvelApiCache
  alias TenExTakeHome.URLData
  alias Task
  alias TenExTakeHome.Events

  def get_characters(pagination) do
    {limit, offset} = pagination
    case MarvelApiCache.get_page_from_cache(limit, offset) do
      {:ok, cached_data} ->
        {:ok, cached_data}

      :error ->
        url = build_url(limit, offset)

        case HTTPoison.get(url) do
          {:ok, %{status_code: 200, body: body}} ->
            {:ok, parsed_body} = Jason.decode(body)
            data = parsed_body["data"]["results"]

            # Caching page result
            MarvelApiCache.put_page_in_cache(limit, offset, data)
            # Create an event to capture api request timesptamp
            create_event_record_async(url)

            {:ok, data}

          {:ok, %{status_code: status_code, body: body}} ->
            {:error, "Request failed with status code #{status_code} and body #{body}"}

          {:error, error} ->
            {:error, "Request failed with error #{inspect(error)}"}
        end
    end
  end

  defp build_url(limit, offset) do
    @base_url <> "?limit=#{limit}&offset=#{offset}&" <> authenticated_params()
  end

  defp authenticated_params() do
    pk = System.get_env("MARVEL_PUBLIC_KEY")
    sk = System.get_env("MARVEL_SECRET_KEY")
    ts = System.system_time()

    hash = :crypto.hash(:md5, "#{ts}#{sk}#{pk}") |> Base.encode16(case: :lower)
    "ts=#{ts}&apikey=#{pk}&hash=#{hash}"
  end

  defp create_event_record_async(url) do
    Task.start(fn ->
      url
      |> URLData.to_map()
      |> Events.create_event_record()
    end)
  end
end
