defmodule TenExTakeHome.MarvelApiCache do
  @cache_name :marvel_api_cache
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{}, name: MarvelApiCache)
  end

  def init(state) do
    :ets.new(@cache_name, [:set, :public, :named_table])
    {:ok, state}
  end

  def get_page_from_cache(limit, offset) do
    case :ets.lookup(@cache_name, {limit, offset}) do
      [{{^limit, ^offset}, data}] -> {:ok, data}
      _ -> :error
    end
  end

  def put_page_in_cache(limit, offset, data) do
    :ets.insert(@cache_name, {{limit, offset}, data})
  end
end
