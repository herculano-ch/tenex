defmodule TenExTakeHomeWeb.CharacterController do
  use TenExTakeHomeWeb, :controller

  def index(conn, params) do
    {:ok, characters} = TenExTakeHome.MarvelApiClient.get_characters(set_pagination(params))

    render(conn, "index.html", characters: characters)
  end

  defp set_pagination(params) do
    {Map.get(params, "limit", "20"), Map.get(params, "offset", "0")}
  end
end
