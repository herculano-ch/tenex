defmodule TenExTakeHomeWeb.CharacterHTML do
  use TenExTakeHomeWeb, :html

  embed_templates "character_html/*"
end
