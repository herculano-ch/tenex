defmodule TenExTakeHome.Repo.Migrations.CreateEventRecords do
  use Ecto.Migration

  def change do
    create table(:event_records) do
      add :api, :string
      add :uri, :string
      add :page, :string
      add :accessed_at, :naive_datetime

      timestamps()
    end
  end
end
